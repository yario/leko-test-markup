import $ from 'jquery';
// import whatInput from 'what-input';

// window.$ = $;

// import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
// import './lib/foundation-explicit-pieces';


// $(document).foundation();

function FormValidation(opt){
  var self = this;
  self.valid = true;
  self.opt = {};

  self.init = function(opt){
    $.extend(self.opt, opt);
    self.events();
  };

  // установка состояния кнопки отправки
  self.setSubmitState = function(state){
    $(self.opt.form).find(self.opt.submit).prop('disabled', state);
  };

  // валидация все необходимых полей (есть вариант с сообщением об ошибке и без)
  self.validation = function(setClass){
    self.valid = true;
    var setClass = setClass | false;
    $.each(self.opt.rules, function(name, element){
      var target = null;
      // определение типа для правильного получения текущего значения
      if (this.type == 'text') {
        target = $(self.opt.form).find('*[name="'+ name +'"]');
      }else if (this.type == 'radio') {
        target = $(self.opt.form).find('*[name="'+ name +'"]:checked');
      };
      var valid = self.validationField(target, target.val(), this, setClass);
      self.valid = !valid ? valid : self.valid;
    });
    self.setSubmitState(!self.valid);
  };

  // валидация поля
  self.validationField = function(field, val, rule, setClass){
    var valid = true;
    var setClass = setClass | false;
    if (rule.required) {
      if (val == '') {
        valid = false;
      }else{
        if (val == rule.condition) {
          valid = false;
        }else{
          valid = true;
        }
      }
      if (setClass) {
        field.closest(rule.closest).toggleClass(this.opt.errorClass, !valid);
        var msg = !valid ? rule.message : '';
        field.closest(rule.closest).find('.js-error-msg').html('<div class="error-msg">' + msg + '</div>');
      }
    };
    return valid;
  };

  // события формы
  self.events = function(){
    var self = this;
    // отправка формы
    $(self.opt.form).on('click', self.opt.submit, function(event){
      event.preventDefault();
      self.validation(true);
    });
    // уход с текстового поля
    $(self.opt.form).on('blur', 'input[type="text"]', function(event){
      if (self.opt.rules[this.name]) {
        self.validationField($(this), $(this).val(), self.opt.rules[this.name], true);
        self.validation();
      }
    });
    // изменение радио-кнопки
    $(self.opt.form).on('change', 'input[type="radio"]', function(event){
      if (self.opt.rules[this.name]) {
        self.validationField($(this), $(this).filter(':checked').val(), self.opt.rules[this.name], true);
        self.validation();
      }
    });
  };

  this.init(opt);
  return this;
};

var claimForm = new FormValidation({
  'form': '.js-form',
  'submit': '.js-submit',
  'errorClass': 'no-valid',
  'rules': {
    'name': {
      'required': true,
      'message': 'Поле обязательно для заполнения',
      'condition': '',
      'closest': '.js-form-input-box',
      'type': 'text'
    },
    'surname': {
      'required': true,
      'message': 'Поле обязательно для заполнения',
      'condition': '',
      'closest': '.js-form-input-box',
      'type': 'text'
    },
    'patronymic': {
      'required': true,
      'message': 'Поле обязательно для заполнения',
      'condition': '',
      'closest': '.js-form-input-box',
      'type': 'text'
    },
    'citizenship': {
      'required': true,
      'message': 'Кредит может быть выдан только гражданам РФ',
      'condition': 'no',
      'closest': '.js-form-input-box',
      'type': 'radio'
    }
  }
});
